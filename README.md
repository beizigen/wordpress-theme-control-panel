# WordPress主题控制类库

支持常见设置项类型：文本框、下拉选择框、复选框、单选框、文本域和图片上传。

## 开始使用

在文章末尾下载这个类文件，将include文件夹拷贝到你的主题目录里，该文件夹中仅包含一个文件：ThemeSetting.php

在你的主题文件functions.php的开头添加以下代码：

```php
require('include/ThemeSetting.php');
```

如此便已经成功引用了这个类。

## 添加页面

可以为主题后台控制面板制作欢迎页面，在主题文件functions.php中添加如下代码：

```php
$themeControlOptions = array(
	'welcome' => array(
		'id' => 'welcome',
		'heading' => '欢迎',
		'type' => 'page',
		'content' => '<p>欢迎使用背字根主题...</p>',
	),
);
$themeControl = new ThemeSetting();
$themeControl->options = $themeControlOptions;
$themeControl->themeControl();
```

#### 参数说明

* id：选项卡的id，需要唯一性；
* heading：选项卡名称；
* type：类型，page表示输出一个页面，页面内容由content决定；
* content：页面内容，支持HTML标签。

## 添加幻灯片

事实上可以添加任何需要上传图片的设置项，但通常幻灯片用得比较多。

```php
$themeControlOptions = array(
	'slide' => array(
		'id' => 'slide',
		'heading' => '幻灯片',
		'type' => 'slide',
		'name' => 'theme_slide',
		'width' => 200,
		'height' => 80,
	),
);
$themeControl = new ThemeSetting();
$themeControl->options = $themeControlOptions;
$themeControl->themeControl();
```

#### 参数说明

* id：选项卡的id，需要唯一性；
* heading：选项卡名称；
* type：类型，slide表示幻灯片功能；
* name：表单name的值，前台输出时需要用到这个值，具有唯一性；
* width：后台显示幻灯片的宽度，不影响前台输出，默认值：320；
* height：后台显示幻灯片的高度，不影响前台输出，默认值：80；

#### 前台调用

幻灯片的数据序列化后保存在wp_option表中，option_name的值为参数说明中的name值。调用示例：

```php
$slides = get_option('theme_slide');
$slides = unserialize($slides);
foreach($slides as $slide) {
	echo '<li><a href="' . $slide['link'] . '" target="_blank"><img src="' . $slide['img'] . '" alt="' . $slide['alt'] . '" /></a></li>';
}
```

以上代码会输出一个li列表。

## 常规设置项

这里统一说明添加文本框（input）、文本域（textarea）、下拉选择框（select）、复选框（checkbox）、单选框（radio）、上传图片的方法。

```php
$themeControlOptions = array(
	'routine' => array(
		'id' => 'routine',
		'heading' => '常规',
		'dsc' => '<p>功能描述</p>',
		'feild' => array(
			'theme_logo' => array(
				'type' => 'file',
				'label' => '网站LOGO',
				'name' => 'theme_logo',
				'width' => 128,
				'height' => 60,
				'default' => get_bloginfo('template_url') . '/img/logo.png',
			),
			'home_title' => array(
				'type' => 'input',
				'label' => '首页SEO标题',
				'name' => 'home_title',
				'dsc' => '首页title标签的内容，只显示在首页title标签中',
				'placeholder' => '填写网站标题',
			),
			'home_description' => array(
				'type' => 'textarea',
				'label' => '首页SEO描述',
				'name' => 'home_description',
				'dsc' => '首页description描述标签的内容，只显示在首页description标签中',
			),
			'link_open' => array(
				'type' => 'radio',
				'label' => '友情链接开关',
				'name' => 'link_open',
				'option' => array(
					'yes' => '是',
					'no' => '否',
				),
			),
			'cat_list' => array(
				'type' => 'checkbox',
				'label' => '首页显示分类',
				'name' => 'cat_list',
				'option' => array(
					'1' => '开发',
					'3' => '资源',
					'2' => '运营',
				),
			),
			'pic_cat' => array(
				'type' => 'select',
				'label' => '图片分类',
				'name' => 'pic_cat',
				'option' => array(
					'1' => '开发',
					'3' => '资源',
					'2' => '运营',
				),
			),
		),
	),
);
$themeControl = new ThemeSetting();
$themeControl->options = $themeControlOptions;
$themeControl->themeControl();
```

#### 参数说明

* id：选项卡的id，需要唯一性；
* heading：选项卡名称；
* dsc：功能说明，支持HTML标签；
* feild：一个数组，包含了各个设置项。

#### feild的参数说明

* type：表单类型，支持：input、textarea、radio、checkbox、select、file；
* label：选项的标签名称；
* name：表单的name属性值；
* dsc：选项的简短描述，不支持HTML标签；
* default：表单默认值；
* placeholder：仅适用于input；
* size：input元素的size属性值，默认值：40；
* cols：textarea元素的cols属性值，默认值：50；
* rows：textarea元素的rows属性值，默认值：5；
* class：input和textarea元素的class值，默认为：large-text；
* width：当type为file时，指定后台显示的图片宽度，不影响前台显示，默认值：80；
* height：当type为file时，指定后台显示的图片高度，不影响前台显示，默认值：80；
* option：当type为radio、checkbox、select时，option指定选项的值和显示名称，option可以是一个键值对的数组，也可以是逗号分隔的字符串值（将用explode函数转换为数组），需要注意保存到数据时取的是该数组的key。

#### 前台调用

数据保存在wp_option表中，option_name的值为参数说明中name的值，调用示例：

```php
get_option('home_title');
```

复选框由于可以选择多个值，保存到数据库时将值以半角逗号分隔保存，所以取出时可以用explode函数转为数组：

```php
$cat_list = get_option('cat_list');
$cat_list = explode(',', $cat_list);
```

以上示例完整代码如下：

```php
require('include/ThemeSetting.php');
$themeControlOptions = array(
	'welcome' => array(
		'id' => 'welcome',
		'heading' => '欢迎',
		'type' => 'page',
		'content' => '<p>欢迎使用背字根主题...</p>',
	),
	'routine' => array(
		'id' => 'routine',
		'heading' => '常规',
		'dsc' => '<p>功能描述</p>',
		'feild' => array(
			'theme_logo' => array(
				'type' => 'file',
				'label' => '网站LOGO',
				'name' => 'theme_logo',
				'width' => 128,
				'height' => 60,
				'default' => get_bloginfo('template_url') . '/img/logo.png',
			),
			'home_title' => array(
				'type' => 'input',
				'label' => '首页SEO标题',
				'name' => 'home_title',
				'dsc' => '首页title标签的内容，只显示在首页title标签中',
				'placeholder' => '填写网站标题',
			),
			'home_description' => array(
				'type' => 'textarea',
				'label' => '首页SEO描述',
				'name' => 'home_description',
				'dsc' => '首页description描述标签的内容，只显示在首页description标签中',
			),
			'link_open' => array(
				'type' => 'radio',
				'label' => '友情链接开关',
				'name' => 'link_open',
				'option' => array(
					'yes' => '是',
					'no' => '否',
				),
			),
			'cat_list' => array(
				'type' => 'checkbox',
				'label' => '首页显示分类',
				'name' => 'cat_list',
				'option' => array(
					'1' => '开发',
					'3' => '资源',
					'2' => '运营',
				),
			),
			'pic_cat' => array(
				'type' => 'select',
				'label' => '图片分类',
				'name' => 'pic_cat',
				'option' => array(
					'1' => '开发',
					'3' => '资源',
					'2' => '运营',
				),
			),
		),
	),
	'slide' => array(
		'id' => 'slide',
		'heading' => '幻灯片',
		'type' => 'slide',
		'name' => 'theme_slide',
		'width' => 200,
		'height' => 80,
	),
);
$themeControl = new ThemeSetting();
$themeControl->options = $themeControlOptions;
$themeControl->themeControl();
```

## 文章元素数据

有时候我们需要为文章添加一个类似于SEO关键词设置的选项，或者其他一些元数据，示例如下：

```php
$postMetaOptions = array(
	'id' => 'seo-setting',
	'heading' => 'SEO设置',
	'screen' => 'post',
	'context' => 'side',
	'priority' => 'high',
	'style' => 'v',
	'upload' => false,
	'feild' => array(
		'seo_title' => array(
			'type' => 'input',
			'label' => 'SEO标题',
			'name' => 'seo_title',
		),
		'seo_description' => array(
			'type' => 'textarea',
			'label' => 'SEO描述',
			'name' => 'seo_description',
		),
	),
);
$postMeta = new ThemeSetting();
$postMeta->options = $postMetaOptions;
$postMeta->postMeta();
```

#### 参数说明

* id：盒子的id，需要唯一性；
* heading：盒子的标题；
* screen：值为post时，在文章编辑页面添加元数据，为page时，则在页面添加；
* context：盒子的位置，可选的值有：normal、side、advanced；
* priority：优先级，盒子的排序规则，可选的值有：high、low；
* style：表单排列方式，v为垂直排列，h为水平排列；
* upload：如果需要上传图片，那么需要指定upload的值为true以便加载支持上传的JS代码；
* feild：请参考前文的说明。

#### 前台调用

文章元数据的前台调用方法如下：

```php
get_post_meta($post->ID, 'seo_title', true);
```

## 页面元数据

页面元数据与文章元数据使用方法相同，只需type属性指定为page即可，也可以使用如下方法：

```php
$postMeta->pageMeta();
```

## 分类元数据

分类不仅需要SEO的一些设置项，有时还需要封面图片设置，示例如下：

```php
$catMetaOptions = array(
	'upload' => false,
	'feild' => array(
		'seo_title' => array(
			'type' => 'input',
			'label' => 'SEO标题',
			'name' => 'seo_title',
		),
		'seo_description' => array(
			'type' => 'textarea',
			'label' => 'SEO描述',
			'name' => 'seo_description',
		),
	),
);
$catMeta = new ThemeSetting();
$catMeta->options = $catMetaOptions;
$catMeta->catMeta();
```

#### 参数说明

* upload：如果需要上传图片，那么需要指定upload的值为true以便加载支持上传的JS代码；
* feild：请参考前文的说明。

#### 前台调用

在前台获取到分类或标签ID就可以调用后台的设置项：

```php
get_term_meta( $tag->term_id, 'seo_title', true );
```
